module hassio-addon-sma

go 1.15

require (
	github.com/eclipse/paho.mqtt.golang v1.2.0
	github.com/spf13/cast v1.3.1
	gitlab.com/bboehmke/sunny v0.15.1-0.20231026142210-2b43d0cd0d7b
	golang.org/x/net v0.0.0-20201110031124-69a78807bb2b // indirect
)
