#!/usr/bin/with-contenv bashio
set -e

CONFIG_PATH=/data/options.json

export CONF_USER_PASSWORD="$(bashio::config 'user_password')"
export CONF_UPDATE_INTERVAL="$(bashio::config 'update_interval')"

export MQTT_HOST=$(bashio::services mqtt "host")
export MQTT_USER=$(bashio::services mqtt "username")
export MQTT_PASSWORD=$(bashio::services mqtt "password")

bashio::log.info "Starting SMA MQTT bridge..."

exec /app/main
