package main

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"sync"
	"time"

	mqtt "github.com/eclipse/paho.mqtt.golang"
	"github.com/spf13/cast"
	"gitlab.com/bboehmke/sunny"
)

func main() {

	conn, err := sunny.NewConnection(os.Getenv("CONF_INTERFACE"))
	if err != nil {
		panic(err)
	}

	opts := mqtt.NewClientOptions()
	opts.AddBroker(fmt.Sprintf("tcp://%s:1883", os.Getenv("MQTT_HOST")))
	opts.SetClientID("home_assistant_sma")
	opts.SetUsername(os.Getenv("MQTT_USER"))
	opts.SetPassword(os.Getenv("MQTT_PASSWORD"))

	c := mqtt.NewClient(opts)
	if token := c.Connect(); token.Wait() && token.Error() != nil {
		panic(token.Error())
	}

	app := App{
		conn:           conn,
		Password:       os.Getenv("CONF_USER_PASSWORD"),
		UpdateInterval: cast.ToInt(os.Getenv("CONF_UPDATE_INTERVAL")),

		Client:  c,
		Devices: make(map[uint32]*Device),
	}
	if app.Password == "" {
		app.Password = "0000"
	}
	if app.UpdateInterval < 1 {
		app.UpdateInterval = 1
	}

	app.Discover()
	for range time.Tick(time.Second * 60) {
		app.Discover()
	}
}

type App struct {
	conn           *sunny.Connection
	Password       string
	UpdateInterval int

	Client mqtt.Client

	Devices map[uint32]*Device
	mutex   sync.RWMutex
}

// Discover SMA devices in connected network
func (a *App) Discover() {
	devices := a.conn.SimpleDiscoverDevices(a.Password)

	a.mutex.Lock()

	for _, dev := range devices {
		// only add device if it is not currently added
		if _, exist := a.Devices[dev.SerialNumber()]; !exist {
			device := &Device{
				Device: dev,
				Client: a.Client,
			}

			name, err := dev.GetValue(sunny.DeviceName)
			if err != nil {
				log.Print(err)
				continue
			}
			device.Name = cast.ToString(name)

			// start device update loop
			go device.UpdateLoop()

			// add device to known devices
			a.Devices[dev.SerialNumber()] = device
			log.Printf("Found device %s @ %s", device.Name, dev.Address())
		}
	}

	a.mutex.Unlock()
}

type Device struct {
	*sunny.Device

	Name  string
	Class uint32

	Client mqtt.Client
}

// UpdateLoop runs infinite and updated the device values
func (d *Device) UpdateLoop() {
	// device data
	deviceData := map[string]interface{}{
		"identifiers":  fmt.Sprintf("sma_%d", d.SerialNumber()),
		"manufacturer": "SMA",
		"name":         d.Name,
	}

	// list of known values
	knownValues := make(map[sunny.ValueID]sunny.ValueDescription)

	for range time.Tick(time.Second) {
		values, err := d.GetValues()
		if err != nil {
			continue // errors are ignored here
		}

		for key, value := range values {
			baseTopic := fmt.Sprintf("homeassistant/sensor/sma/%d_%s", d.SerialNumber(), key.String())

			// add MQTT discovery for new values
			if _, exists := knownValues[key]; !exists {
				data := map[string]interface{}{
					"device":      deviceData,
					"name":        fmt.Sprintf("SMA %s %s", d.Name, key.String()),
					"state_topic": baseTopic + "/data",
					"unique_id":   fmt.Sprintf("sma_%d_%s", d.SerialNumber(), key.String()),
					"state_class": "measurement",
				}

				// get information about value
				info := sunny.GetValueInfo(key)
				if info.Type != "" {
					data["device_class"] = info.Type
				}
				if info.Unit != "" {
					data["unit_of_measurement"] = info.Unit
				}

				// publish value as new MQTT sensor
				jsonData, _ := json.MarshalIndent(data, "", "  ")
				token := d.Client.Publish(baseTopic+"/config", 1, true, jsonData)
				if token.Wait() && token.Error() != nil {
					log.Print(token.Error())
					continue
				}
				knownValues[key] = info
			}

			// update sensor value
			token := d.Client.Publish(baseTopic+"/data", 0, false, cast.ToString(value))
			if token.Wait() && token.Error() != nil {
				log.Print(token.Error())
			}
		}
	}
}
